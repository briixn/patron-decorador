interface Cerveceria {
    fun servirCervezaNormal()
    fun servirCervezaArtesanal()
}

open class CerveceriaNormal : Cerveceria {
    override fun servirCervezaNormal() = println("Preparando una cerveza club o pilsener")

    override fun servirCervezaArteesanal() = println("Preparando una cerveza artesanal")
}

//Aplicndo el Decorator:
open class CerveceriaEnGeneral(val cerveceria: Cerveceria) : Cerveceria by cerveceria {


    override fun servirCervezaArtesanal() {
        println("Preparando una cerveza artesanal")
    }

    // Mas y nuevo comportamiento
    fun hacerMicheladaMaracuya() {
        println("Realizando una michelada de maracuya")
        cerveceria.servirCervezaNormal()
        agregarMaracuya()
    }

    private fun agregarMaracuya() {
        println("Agregando la maracuya")
    }
}

class TestCervezas {


    fun main() {
        //probando decorador
        val cervezaNormal = CerveceriaNormal()
        val cervezaEspecial = CerveceriaEnGeneral(cervezaNormal)


        cervezaNormal.servirCervezaNormal()
        // otro comportamiento
        cervezaEspecial.servirCervezaArtesanal()
        // comportamiento agregado
        cervezaEspecial.hacerMicheladaMaracuya()
    }
}